# NAME

shl_award_qualify - Modifies Stronghold Legends so that you can earn awards without actually having qualified for them.

## DEPENDENCIES

No special dependencies, regular POSIX libraries for Linux

## DESCRIPTION

Launch this program first, then just start up Stronghold Legends from Steam. In the terminal window for this program, you can select which award to unlock. The next time you complete a match in the game, you will get the award.

You will also get the associated Steam Achievement.

### HOW IT WORKS

After a match, the game code checks if your current profile already has an award. If not, it will check if you qualify, and then give you the award if you do qualify. This program circumvents this by letting you get the award even if you don't qualify. Technically, there are several conditional jumps in the game code that ensure you don't get the award if you don't qualify. This program overwrites all those conditional jumps with NOP codes (0x90).

These change are all temporary, no game files are modified.

## BUILDING

As with any compilation, you will need a toolchain properly set up. On Linux, just check with your distributions documentation. For Windows, my personal recommendation will always be MSYS2.

There is a Makefile available for easy building on both Linux and MSYS2 on Windows.

You can also use Visual Studio to compile. I have never tested it myself, but should work.

On Windows you don't need to include the gnu\_linux\_funcs.c source when compiling.

On Linux you don't need to include the ms\_funcs.c source when compiling. However, you do need to compile with the macro STRONGHOLD\_ON\_GNU\_LINUX defined.

### BUILDING WITH MAKEFILE

On Linux, all you need to do is 'cd' into the src directory and run 'make'. This will produce the binary 'shl\_award\_qualify'

On Windows in MSYS2, the procedure is almost the same. 'cd' into the src directory and run 'make windows'. This will produce the binary 'shl\_award\_qualify.exe'

You can provide additional CFLAGS and LDFLAGS if desired. Example: `make CFLAGS="-O2"`

## AUTHOR

Daniel Jenssen <daerandin@gmail.com>

